package com.tutorial.dao;

import org.springframework.data.repository.CrudRepository;

import com.tutorial.model.UserDetails;

public interface UserDao extends CrudRepository<UserDetails, Long> {
	
	UserDetails findByEmail(String email);
	
}
